#!/bin/bash

# Remove all old packages
reprepro -V removefilter canary 'Section'

# Add new packages
reprepro -S main -P extra includedeb canary ../su-packages/debs/*.deb
